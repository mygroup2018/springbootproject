package com.dummyproject.dummyservices.services;

import com.dummyproject.dummyservices.mappers.MyBatisUtil;
import com.dummyproject.dummyservices.mappers.UserRepositary;
import com.dummyproject.dummyservices.model.UserModel;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    UserRepositary userRepositary;

    private SqlSession sqlSession;

    public UserService() {
        this(MyBatisUtil.getSqlSessionFactory().openSession());
    }

    public UserService(SqlSession sqlSession) {
        this.sqlSession= sqlSession;
        this.userRepositary = sqlSession.getMapper(UserRepositary.class);
    }

    public UserModel addStudent(UserModel user) {
        userRepositary.save(user);
        sqlSession.commit();
        return user;
    }

    public String fetchName() {
        String name = userRepositary.fetch();
        sqlSession.commit();
        return name;
    }
}

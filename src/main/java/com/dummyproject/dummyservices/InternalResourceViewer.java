package com.dummyproject.dummyservices;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class InternalResourceViewer implements WebMvcConfigurer {
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry){
        InternalResourceViewResolver ivc = new InternalResourceViewResolver();
        ivc.setPrefix("/jsp/");
        ivc.setSuffix(".jsp");
        registry.viewResolver(ivc);
    }

}

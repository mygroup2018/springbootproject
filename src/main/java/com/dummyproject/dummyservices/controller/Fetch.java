package com.dummyproject.dummyservices.controller;

import com.dummyproject.dummyservices.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Fetch {
    @RequestMapping("/fetch")
    public String fetchData(Model model){
        UserService userService = new UserService();
        String name = userService.fetchName();
        model.addAttribute("fetched_name",name);
        return "displayFetchedData";
    }
}

package com.dummyproject.dummyservices.controller;

import com.dummyproject.dummyservices.model.UserModel;
import com.dummyproject.dummyservices.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Display {
    @RequestMapping("/display")
    public String displayName(@RequestParam String name, Model model){
        UserModel user = new UserModel();
        user.setName(name);
        UserService userService = new UserService();
        userService.addStudent(user);
        model.addAttribute("name",name);
        return "display";
    }
}

package com.dummyproject.dummyservices.mappers;

import com.dummyproject.dummyservices.model.UserModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface UserRepositary{
    @Insert(
            "INSERT INTO user1 (name) " +
                    "VALUES (#{name})"
    )
    Integer save(UserModel user);

    @Select(
            "SELECT name FROM user1 WHERE name = 'fifth' "
    )
    String fetch();
}

echo "------------- copying artifact to the container ------------"
scp ../build/libs/dummy-services-0.0.1-SNAPSHOT.jar vagrant@192.168.33.10:/home/vagrant/artifacts

echo "------------- running jar file in container -------------"
echo "
   pwd
   ls
   cd artifacts
   nohup java -jar dummy-services-0.0.1-SNAPSHOT.jar > /dev/null &
" | ssh vagrant@192.168.33.10 /bin/bash